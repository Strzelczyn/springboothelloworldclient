package org.example;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


public class Main {
    public static final String API = "http://localhost:8080/";

    public static void main(String[] args) {
        Main main = new Main();
        main.postHelloWorld();
        main.getHelloWorld();
    }

    private void getHelloWorld() {
        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<String> claimResponse = restTemplate.exchange(
                    API +"/gets",
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<String>() {});
            if(claimResponse != null && claimResponse.hasBody()){
                System.out.println("GET: " + claimResponse.getBody());
            }
        } catch (RestClientException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void postHelloWorld() {
        RestTemplate restTemplate = new RestTemplate();
        String claimResponse = restTemplate.postForObject(
                API+"/posts",
                "Hello World",
                String.class);
        System.out.println("POST: " + claimResponse);
    }
}